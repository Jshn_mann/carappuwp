﻿using System;

namespace CarAppLibrary
{
    public class Car // created  class with field variables
    {
        private string _vinNumber;
        private string _carMake;
        private CarType _carType;
        private float _purchasePrice;
        private int _modelYear;
        private int _mileage;
        private float _totalDepreciation;

        public override string ToString()
        {
            return $"{VinNumber}, {CarMake}, {PurchasePrice},{ ModelYear},{Mileage}, {TotalDepreciation}";
        }

        public Car(string vinNumber, string carMake, CarType carType, float purchasePrice, int modelYear, int mileage) // constructor added
        {
            this.VinNumber = vinNumber; // "this" is refrence to car class
            CarMake = carMake; // invoking a property 
            PurchasePrice = purchasePrice;
            CarType1 = carType;
            ModelYear = modelYear;
            Mileage = mileage;
        }


        public string VinNumber // read and write property for vin number
        {
            get
            {
                return _vinNumber;
            }
            private set
            {
                if (string.IsNullOrEmpty(value)) // adding exception according to condition
                    throw new Exception("VIN Number cannot be blank");
                _vinNumber = value; // if no exception occur
            }
        }

        public string CarMake // read and write property for car make
        {
            get { return _carMake; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Car Make cannot be blank");
                _carMake = value;
            }
        }


        public float PurchasePrice // read and write property for Purchase price
        {
            get { return _purchasePrice; }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Purchase Price must be greater than 0");
                }
                _purchasePrice = value;
            }
        }

        public int ModelYear // read and write property for Model Year
        {
            get { return _modelYear; }
            set
            {
                _modelYear = value; 
            }
        }

        public int Mileage // read and write property for mileage
        {
            get { return _mileage; }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Mileage must be greater than 0");
                }
                _mileage = value;
            }
        }

        public CarType CarType1
        {
            get { return _carType; }
            set
            {
                _carType =value;
            }

        }

        public float TotalDepreciation //computed property for calculating deprecation at given rate
        {
            get {
                return _totalDepreciation = (float) (((2020 - _modelYear) * 0.1 * _purchasePrice) + (_mileage * 0.9 * 0.01 * 0.0001 * _purchasePrice));
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarAppLibrary
{
    public class CarRepository
    {

        // hard coded cars using contructor
        Car car1 = new Car("JASH26092001", "Mercedes", CarType.Sedan, 98000, 2019, 10000);
        Car car2 = new Car("ABCD01012001", "GMC", CarType.PickUpTruck, 53000, 2015, 15000);
        Car car3 = new Car("DALJ12041979", "AUdi", CarType.SUV, 110000, 2017, 20000);

        private List<Car> _cars = new List<Car>(); // list of cars


        //adding car
        public void AddCar(Car c)
        {
            //add a check to ensure product code is not duplicate
            if (GetByNumber(c.VinNumber) != null)
            {
                throw new Exception("Car already exists.");
            }
            _cars.Add(c);
        }

        //search by car vin number
        public Car GetByNumber(string vinNumber)
        {
            foreach (Car c in _cars)
            {
                if (c.VinNumber == vinNumber)
                {
                    return c;
                }
            }
            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using CarAppLibrary;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace CarAppUWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        private CarRepository _carRepository = new CarRepository();
        public MainPage()
        {

            this.InitializeComponent();
            CmbCarType.ItemsSource = Enum.GetValues(typeof(CarType));
        }

        private Car CaptureCarInfo()
        {
            string vinNumber = TxtVinNumber.Text;
            string carMake = TxtCarMake.Text;
            CarType carType = (CarType)CmbCarType.SelectedItem;
            float purchasePrice = float.Parse(TxtPurchasePrice.Text);
            int modelYear = int.Parse(CmbModelYear.SelectedItem.ToString());
            int mileage = int.Parse(TxtMileage.Text);
            Car c = new Car(vinNumber, carMake, carType, purchasePrice, modelYear, mileage);
            return c;
        }

        private void RenderCarInfo(Car car)
        {
            TxtVinNumber.Text = car.VinNumber;
            TxtCarMake.Text = car.CarMake;
            CmbCarType.SelectedItem = car.CarType1.ToString();
            TxtPurchasePrice.Text = car.PurchasePrice.ToString();
            CmbModelYear.SelectedItem = car.ModelYear.ToString();
            TxtMileage.Text = car.Mileage.ToString();

            if (CmbCarType.SelectedIndex == 0)
            {
                ImgcarType.Source = new BitmapImage(new Uri($"ms-appx:///Assets/Car/vinNumber01.jpg"));
            }
            if (CmbCarType.SelectedIndex == 1)
            {
                ImgcarType.Source = new BitmapImage(new Uri($"ms-appx:///Assets/Car/vinNumber02.jpg"));
            }
            if (CmbCarType.SelectedIndex == 2)
            {
                ImgcarType.Source = new BitmapImage(new Uri($"ms-appx:///Assets/Car/vinNumber03.jpg"));
            }
            if (CmbCarType.SelectedIndex == 3)
            {
                ImgcarType.Source = new BitmapImage(new Uri($"ms-appx:///Assets/Car/vinNumber04.jpg"));
            }
            if (CmbCarType.SelectedIndex == 4)
            {
                ImgcarType.Source = new BitmapImage(new Uri($"ms-appx:///Assets/Car/vinNumber05.jpg"));
            }
            if (CmbCarType.SelectedIndex == 5)
            {
                ImgcarType.Source = new BitmapImage(new Uri($"ms-appx:///Assets/Car/vinNumber06.jpg"));
            }

        }

        private void ClearUI()
        {
            TxtVinNumber.Text = "";
            TxtCarMake.Text = "";
            CmbCarType.SelectedItem = "";
            TxtPurchasePrice.Text = "";
            CmbModelYear.SelectedItem = "";
            TxtMileage.Text = "";
            TxtErrMessage.Text = "";
        }

        private void OnAddCar(object sender, RoutedEventArgs e)
        {
            try
            {

                Car c = CaptureCarInfo();
                _carRepository.AddCar(c);
                LstCars.Items.Add(c);
            }
            catch (Exception ex)
            {
                TxtErrMessage.Text = ex.Message;
            }
        }

        private void OnClear(object sender, RoutedEventArgs e)
        {
            ClearUI();
        }

        private void OnCarSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //1. get the selected car
            Car c = (Car)LstCars.SelectedItem;

            //2. render the fields in the textboxes
            RenderCarInfo(c);
        }


    }
}
